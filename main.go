package main

import "fmt"

var fileName string

func main() {
	deck := newDeck()
	fileName = "default_deck.txt"
	deck.storeToDisk(fileName)
	// byteSlice := []byte(deck.toString())

	// fmt.Println(byteSlice)
	// fmt.Println(string(byteSlice))

	hand, deck := deck.deal(5)
	hand.printAll()
	hand, deck = deck.deal(5)
	hand.printAll()
	hand, deck = deck.deal(5)
	hand.printAll()
	hand, deck = deck.deal(5)
	hand.printAll()
	hand, deck = deck.deal(5)
	hand.printAll()
	fmt.Println("Deck print")
	deck.printAll()

	fmt.Println("Default deck")
	deck = deck.loadDeckFromDisk(fileName)

	deck.printAll()
	deck.shuffle()
	deck.printAll()

}

func newCard(title string) string {
	return title
}
