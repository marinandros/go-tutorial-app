package main

import (
	"os"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := newDeck()
	deckSize := len(d)

	if deckSize != 52 {
		t.Errorf("Expected deck length of 52, but got: %d", deckSize)
	}

	firstElem := d[0]
	if firstElem != "Ace of Hearts" {
		t.Errorf("First card should be 'Ace of Hearts', but got %s", firstElem)
	}

	lastElem := d[len(d)-1]
	if lastElem != "King of Clubs" {
		t.Errorf("Last card should be 'King of Clubs', but got %s", lastElem)
	}
}

func TestSaveToDeckAndNewDeckTestFromFile(t *testing.T) {
	fileName := "_decktesting"
	os.Remove(fileName)

	deck := newDeck()
	deck.storeToDisk(fileName)

	info, err := os.Stat(fileName)
	if err != nil {
		t.Errorf("File '%s' isn't stored on the disk", fileName)
	}
	if info.IsDir() {
		t.Errorf("Given name of '%s' is a directory", fileName)
	}

	deck = deck[2:]

	if len(deck) != 50 {
		t.Errorf("Expected the size of the deck to be 50, got %d", len(deck))
	}

	deck = deck.loadDeckFromDisk("fileName")

	if len(deck) != 52 {
		t.Errorf("Expected the size of the deck to be reset to 52, got %d", len(deck))
	}
	os.Remove(fileName)
}
