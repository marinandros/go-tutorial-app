package main

import (
	"fmt"
	IO "io/ioutil"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

type deck []string

func newDeck() deck {
	newDeck := deck{}
	cardSuites := [...]string{"Hearts", "Spades", "Diamonds", "Clubs"}
	cardVal := [...]string{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"}
	for _, suite := range cardSuites {
		for _, val := range cardVal {
			newDeck = append(newDeck, fmt.Sprintf("%s of %s", val, suite))
		}
	}
	return newDeck
}

func (d deck) print() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

func (d deck) printAll() {
	fmt.Println(d)
}

func (d deck) toString() string {
	stringSLice := []string(d)
	return strings.Join(stringSLice[:], ",")
}

func (d deck) storeToDisk(fileName string) error {
	err := IO.WriteFile(fileName, []byte(d.toString()), 0666)
	return err
}

func (d deck) loadDeckFromDisk(fileName string) deck {
	info, err := os.Stat(fileName)
	if err != nil {
		fmt.Println(err)
		return []string{}
	}
	if info.IsDir() {
		log.Panicln(fmt.Scanf("Provided path `%s` is a directory", err))
		return []string{}
	}

	data, err := IO.ReadFile(fileName)
	if err != nil {
		log.Println(fmt.Scanf("Something went wrong while reading the file:`%s`", err))
		os.Exit(1)
	}
	return deck(strings.Split(string(data), ","))
}

func (d deck) deal(cardsNum int) (deck, deck) {
	if len(d) == 0 {
		return []string{}, d
	}
	if len(d) < cardsNum {
		cardsNum = len(d)
	}
	hand := d[:cardsNum]
	newDeck := d[cardsNum:]
	return hand, newDeck
}

func (d deck) shuffle() deck {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	r.Shuffle(len(d), func(i, j int) {
		d[i], d[j] = d[j], d[i]
	})
	return d
}
